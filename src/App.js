import React, { Component } from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import deepPurple from '@material-ui/core/colors/deepPurple'
import MenuIcon from '@material-ui/icons/Menu'
import Header from './ui/sections/Header/'
import Body from './ui/sections/Body/'

class App extends Component {
  render() {
    const theme = createMuiTheme({
      palette: {
        primary: deepPurple
      }
    })
    const icon = <MenuIcon />
    
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Header icon={icon} title="Messages" />
          <Body />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default App
