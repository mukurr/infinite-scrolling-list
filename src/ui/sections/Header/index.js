import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import './style.css'

class Header extends Component {

  render() {
    const { title, icon } = this.props
    return (
      <AppBar position="fixed" color="primary">
        <Toolbar className="toolbar">
          <IconButton className="headerIcon" color="inherit" aria-label="Menu">{icon}</IconButton>
          <Typography variant="title" color="inherit">{title}</Typography>
        </Toolbar>
      </AppBar>
    )
  }
}

export default Header
