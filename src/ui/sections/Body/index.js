import React, { Component } from 'react'
import axios from 'axios'
import { Debounce, Throttle } from '../../../services/helpers'
import MessageCard from '../../components/MessageCard/'
import MessageCardLoader from '../../components/MessageCard/loader'
import './style.css'

let startPolling = true
const getMessagesUrl = `http://message-list.appspot.com/messages?limit=10`

class Body extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      messages: [],
      pageToken: null,
    }
  }

  componentWillMount = () => {
    this.getMessages();
  }

  componentDidMount = () => {
    window.addEventListener('scroll', Debounce(this.handleInfiniteScroll), 1000)
  }

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.handleInfiniteScroll)
  }

  getMessages = () => {
    let pageToken = this.state.pageToken !== null ? `&pageToken=${this.state.pageToken}` : ""
    axios.get(`${getMessagesUrl}${pageToken}`)
    .then(response => {
      let data = response.data
      if(data.messages.length) {
        const messages = Array.from(this.state.messages);
        data.messages.forEach(message => {
          message.visible = true
          messages.push(message);   
        })
        this.setState({messages, pageToken: data.pageToken}, () => {
          startPolling = true
        })
      }
    })
    .catch(error => {
      console.log(error)
    })
  }

  handleInfiniteScroll = event => {
    if(startPolling === true) {
      if((window.innerHeight + window.pageYOffset + 500) >= (document.body.offsetHeight)) {
        startPolling = false
        this.getMessages()
      }
    }
    /*Throttle(this.optimizeDOM(), 10000)*/
  }

  archiveMessage = id => {
    const messages = Array.from(this.state.messages);
    const newMessages = [];
    for(var i = 0; i < messages.length; i++) {
      if(messages[i].id !== id) {
        newMessages.push(messages[i])
      }
    }
    this.setState({
      messages: newMessages
    })
  }

  optimizeDOM = () => {
    setTimeout(() => {
      let cards = document.getElementsByClassName("card")
      const messages = Array.from(this.state.messages);
      for(let card of cards) {
        if(card.offsetParent) {
          if(card.offsetParent.offsetTop < (window.pageYOffset)) {
            const index = messages.findIndex(obj => obj.id === parseInt(card.id.split('-')[1], 10))
            messages[index].visible = false
          }
        }
      }
      this.setState({
        messages
      })
    }, 2000)
  }

  render() {
    const messageList = [];
    this.state.messages.forEach(message => {
    messageList.push(<MessageCard archive={(id) => {this.archiveMessage(id)}} id={`message-card-${message.id}`} key={message.id} message={message} />) 
    });
    return (
      <div className="container">
        <div className="messageList">
          {messageList}
          <MessageCardLoader size={3} />
        </div>
      </div>
    ); 
  }
}

export default Body
