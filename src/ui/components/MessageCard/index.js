import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Avatar from '@material-ui/core/Avatar'
import { GetPassedTime } from '../../../services/helpers'
import MessageToast from '../../components/MessageToast/'
import './style.css'

let x0 = null

class MessageCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dx: null,
      ellipsisText: props.message.content,
      showtoast: false,
      archiveMessage: true,
    }
  }

  unifyGesture = e => { 
    return e.changedTouches ? e.changedTouches[0] : e
  }

  gestureStart = e => {
    x0 = this.unifyGesture(e).clientX
  }

  gestureMove = e => {
    let dx = this.unifyGesture(e).clientX - x0
    if((x0 || x0 === 0) && dx > 25) {
      let card = e.currentTarget
      card.style.setProperty('left', `${dx}px`)
    } else {
      e.currentTarget.style.setProperty('left', 0)
    }
  }

  gestureEnd = e => {
    let lastTouchPoint = this.unifyGesture(e).clientX, dx = lastTouchPoint - x0, card = e.currentTarget
    if(dx > 40) {
      this.archiveMessage(card)
    } else {
      e.currentTarget.style.setProperty('left', 0)
    }
  }

  archiveMessage = card => {
    card.style.setProperty('left', `${window.innerWidth * 2}px`)
    card.parentElement.style.setProperty('height', card.offsetHeight + "px")
    setTimeout(() => {
      this.setState({
        showtoast: true
      }, () => {
        Object.assign(card.parentElement.style,{
          height:0,
          margin:0,
          padding:0
        })
        x0 = null
        setTimeout(() => {
          this.setState({
            showtoast: false
          }, () => {
            /* Remove Card from DOM, after user rejects undo option for deleting message inside the messageToaster */
            /*if(this.state.archiveMessage) {
              this.props.archive(parseInt(card.id.split("-")[1], 10))
            }*/
          })
        }, 1000)
      })
    })
  }

  undoArchiveMessage = (id) => {
    this.setState({
      archiveMessage: false,
      showtoast: false
    }, () => {
      let card = document.getElementById(`message-${id}`);
      card.parentElement.style.setProperty('height', card.offsetHeight + "px")
      Object.assign(card.parentElement.style,{
        marginBottom:'5px',
        paddingBottom: '5px'
      })
      setTimeout(() => {card.style.setProperty('left', 0)}, 100)
    })
  }

  componentDidMount = () => {
    /* Code to update message content with ellipsis if text */
    /* is greater than little longer to fourth line */
    let lineWidth = parseInt(document.getElementsByClassName("messageContent")[0].offsetWidth, 10)
    let letterCount = parseInt(lineWidth / 12, 10) * 7
    if(this.state.ellipsisText.length > letterCount) {
      let ellipsisText = this.state.ellipsisText.substr(0, letterCount) + "..."
      this.setState({ellipsisText})
    }
  }

  render() {
    const { message } = this.props
    return (
      <div className="messageCard" id={`message-card-${message.id}`} >
        <Card
          id={`message-${message.id}`}

          /*Events for Touch behaviour */
          onTouchStart={this.gestureStart}
          onTouchMove={this.gestureMove}
          onTouchEnd={this.gestureEnd}
          /* --------------------- */
          
          /*Events for mouse behaviour */
          onMouseDown={this.gestureStart}
          onMouseMove={this.gestureMove}
          onMouseUp={this.gestureEnd}
          /* --------------------- */

          className="card"
          >
          <CardContent>
            <div className="profilePlaceholder">
              <Avatar alt={message.author.name} src={`http://message-list.appspot.com${message.author.photoUrl}`} className="avatar"/>
              <div className="profileText">
                <Typography className="profileTitle" >{message.author.name}</Typography>
                <Typography className="messageTime">{GetPassedTime(message.updated)}</Typography>
              </div>
            </div>
            <Typography className="messageContent" component="p">{this.state.ellipsisText}</Typography>
          </CardContent>
        </Card>
        <MessageToast id={message.id} undoarchive={this.undoArchiveMessage} showtoast={this.state.showtoast.toString()} title="1 archived" action="undo" />
      </div>)
  }
}

export default MessageCard
