import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import './style.css'

class MessageCardLoader extends Component {
  render() {
    let messages = []
    for(let i=0 ; i < (this.props.size || 2); i++) {
      messages.push(
        <Card key={i} className="loaderCard">
          <CardContent>
            <div className="profilePlaceholder">
              <div className="loaderAvatar"></div>
              <div className="profileText">
                <div className="profileTitleLoader"></div>
                <div className="messageTimeLoader"></div>
              </div>
            </div>
            <div className="messageContentLoader"></div>
            <div className="messageContentLoader"></div>
            <div className="messageContentLoader"></div>
          </CardContent>
        </Card>)
    }
    return (
      <div>
        {messages}
      </div>
    )
  }
}

export default MessageCardLoader
