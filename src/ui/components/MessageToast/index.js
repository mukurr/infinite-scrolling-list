import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography'
import './style.css'

class MessageToast extends Component {
  
  render() {
    return (
      <div {...this.props} style={{height: this.props.showtoast === 'true' ? '50px' : 0 }} className="messageToast">
        <Typography className="title">{this.props.title}</Typography>
        <Typography onClick={() => this.props.undoarchive(this.props.id)} className="action">{this.props.action.toUpperCase()}</Typography>
      </div>)
  }
}

export default MessageToast
