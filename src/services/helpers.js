const GetPassedTime = date => {
	let passedTime = "",
	today = new Date(),
	pastDate = new Date(date),
	timeDiff = Math.abs(today.getTime() - pastDate.getTime()),
	remainingTimeFormat = ["year","month", "week", "day", "hour", "minute" , "second"],
	secondsPassed = parseInt(timeDiff / 1000, 10),
	minutesPassed = parseInt(secondsPassed / 60, 10),
	hoursPassed = parseInt(minutesPassed / 60, 10),
	daysPassed  = parseInt(hoursPassed / 24, 10) ,
	weeksPassed  = parseInt(daysPassed / 7, 10) ,
	monthsPassed  = parseInt(weeksPassed / 4, 10) ,
	yearsPassed  = parseInt(monthsPassed / 12, 10),
	remainingTimeDate = [
		yearsPassed,
		monthsPassed,
		weeksPassed,
		daysPassed,
		hoursPassed,
		minutesPassed,
		secondsPassed
	],
	minimumTime = remainingTimeDate[0],
	minimumIndex = 0

	remainingTimeDate.forEach(function(element, index) {
		if(element < minimumTime) {
			minimumTime = element
			minimumIndex = index
		}
	})
	
	passedTime = minimumTime + " " + remainingTimeFormat[minimumIndex] + (minimumTime > 1 ? "s ago" : " ago") 

	return passedTime
}

const Debounce = (func, wait, immediate) => {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

const Throttle = (func, limit) => {
  let inThrottle
  return function() {
    const args = arguments
    const context = this
    if (!inThrottle) {
      func.apply(context, args)
      inThrottle = true
      setTimeout(() => inThrottle = false, limit)
    }
  }
}
export { GetPassedTime, Debounce, Throttle }